#!/bin/bash

set -e

RELEASE_EXTENSION="xz"

usage()
{
	echo "Use as make-release [OPTIONS] PACKAGE VERSION TARGET"
	echo "Example: make-release gnutls 3.6.3 f29"
	echo "Example: make-release -b https://github.com/p11-glue/p11-kit/releases/download/0.23.14 -e gz p11-kit 0.23.14 f29"
	exit 1
}

while getopts "b:e:" opt; do
    case "$opt" in
    b)
        RELEASE_URI="$OPTARG"
        ;;
    e)
        RELEASE_EXTENSION="$OPTARG"
        ;;
    -)
        break
        ;;
    *)
        usage
        ;;
    esac
done

shift $(expr $OPTIND - 1)

if [ $# -ne 3 ]; then
    usage
fi

PACKAGE=$1
VERSION=$2
TARGET=$3

# Initial variables which help the scripts share
RELEASE_SOURCE=$PWD/_release/$PACKAGE/$TARGET
RELEASE_UPSTREAM=$PWD/_release/$PACKAGE/upstream
RELEASE_SPEC=$PWD/_release/$PACKAGE.spec
RELEASE_SRPM_DIR=$PWD/_release/srpm
TOOLS_DIR=$PWD
SCRIPTS_DIR=$PWD/cockpituous/release

git submodule update --init

rm -rf $RELEASE_SOURCE
git clone --depth=1 -b "$TARGET" https://src.fedoraproject.org/rpms/$PACKAGE.git $RELEASE_SOURCE
rm -rf $RELEASE_SOURCE/.git
# Divert the spec file out of $RELEASE_SOURCE
sed -n '1,/^%changelog/p' $RELEASE_SOURCE/$PACKAGE.spec > $RELEASE_SPEC
rm -f $RELEASE_SOURCE/$PACKAGE.spec

rm -rf $RELEASE_UPSTREAM
mkdir -p $RELEASE_UPSTREAM

# Download the upstream release
pushd $RELEASE_UPSTREAM
if test -n "$RELEASE_URI"; then
	URI0="$RELEASE_URI/$PACKAGE-$VERSION.tar.$RELEASE_EXTENSION"
	URI1="$RELEASE_URI/$PACKAGE-$VERSION.tar.$RELEASE_EXTENSION.sig"
else
	URI0=$(grep Source0 "$RELEASE_SPEC"|tr -s ' '|cut -d ' ' -f 2|sed -e "s/%{name}/$PACKAGE/g" -e "s/%{version}/$VERSION/g")
	URI1=$(grep Source1 "$RELEASE_SPEC"|tr -s ' '|cut -d ' ' -f 2|sed -e "s/%{name}/$PACKAGE/g" -e "s/%{version}/$VERSION/g")
fi

if ! test -f "$PACKAGE-$VERSION.tar.$RELEASE_EXTENSION";then
	wget ${URI0}
fi

if ! test -f "$PACKAGE-$VERSION.tar.$RELEASE_EXTENSION.sig" && test -n "${URI1}";then
	wget ${URI1}
fi

if test -f "$PACKAGE-$VERSION.tar.$RELEASE_EXTENSION.sig";then
	gpg --verify $PACKAGE-$VERSION.tar.$RELEASE_EXTENSION.sig
fi

# Copy upstream files to source directory
find $RELEASE_UPSTREAM -maxdepth 1 -type f | while read file; do
    cp "$file" ${RELEASE_SOURCE}
done
popd

# Build the source tarball patches and srpm
echo " * Build srpm"

rm -rf $RELEASE_SRPM_DIR
mkdir -p $RELEASE_SRPM_DIR

export RELEASE_SOURCE
export RELEASE_SPEC
export RELEASE_TAG="$VERSION"
export RELEASE_PATCH=1
export RELEASE_TARBALL="$RELEASE_UPSTREAM/$PACKAGE-$VERSION.tar.$RELEASE_EXTENSION"

pushd $RELEASE_SRPM_DIR
$SCRIPTS_DIR/release-srpm -p
popd

RELEASE_SRPM=$(ls $RELEASE_SRPM_DIR/$PACKAGE-*.src.rpm)

echo " * Koji build"
# Do fedora builds for the tag, using tarball
$SCRIPTS_DIR/release-koji -v -p $RELEASE_SRPM $TARGET

#export RELEASE_SRPM
if test "$TARGET" != "master";then
	echo " * Bodhi release"
	$SCRIPTS_DIR/release-bodhi -p ${RELEASE_SRPM_DIR}/${PACKAGE}-${VERSION}*.src.rpm $TARGET
fi
